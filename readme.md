

how to start
============

1. register account:     https://onedrive.live.com/  
2. pip install -r requirements.txt
3. python manage.py migrate
4. python manage.py runserver
5. open http://localhost:8000/tutorial
6. use step-1 account to login
7. click buttons on top and take a look at https://onedrive.live.com/

refer to
========

* https://docs.microsoft.com/zh-cn/graph/tutorials/python
* https://docs.microsoft.com/zh-cn/graph/api/resources/onedrive?view=graph-rest-1.0

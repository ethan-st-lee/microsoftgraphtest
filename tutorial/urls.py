from django.urls import path

from . import views

urlpatterns = [
    # /tutorial
    path("", views.home, name="home"),
    path("signin", views.sign_in, name="signin"),
    path("callback", views.callback, name="callback"),
    path("signout", views.sign_out, name="signout"),
    path("calendar", views.calendar, name="calendar"),
    path("docs", views.docs, name="docs"),
    path("create/dir/", views.create_dir, name="create_dir"),
    path("upload/new/file/", views.upload_new_file, name="upload_new_file"),
]

import json, random
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from tutorial.auth_helper import (
    get_sign_in_url,
    get_token_from_code,
    store_token,
    store_user,
    remove_user_and_token,
    get_token,
)
from tutorial.graph_helper import *
import dateutil.parser
from requests_oauthlib import OAuth2Session


def home(request):
    context = initialize_context(request)

    return render(request, "tutorial/home.html", context)


def initialize_context(request):
    context = {}

    # Check for any errors in the session
    error = request.session.pop("flash_error", None)

    if error != None:
        context["errors"] = []
        context["errors"].append(error)

    # Check for user in the session
    context["user"] = request.session.get("user", {"is_authenticated": False})
    return context


def sign_in(request):
    # Get the sign-in URL
    sign_in_url, state = get_sign_in_url()
    print("state", state)
    # Save the expected state so we can validate in the callback
    request.session["auth_state"] = state
    # Redirect to the Azure sign-in page
    return HttpResponseRedirect(sign_in_url)


def callback(request):
    # Get the state saved in session
    expected_state = request.session.pop("auth_state", "")
    print("expected_state", expected_state)
    # Make the token request
    token = get_token_from_code(request.get_full_path(), expected_state)

    # Get the user's profile
    user = get_user(token)

    # Save token and user
    store_token(request, token)
    store_user(request, user)

    return HttpResponseRedirect(reverse("home"))


def sign_out(request):
    # Clear out the user and token
    remove_user_and_token(request)

    return HttpResponseRedirect(reverse("home"))


def calendar(request):
    context = initialize_context(request)

    token = get_token(request)

    events = get_calendar_events(token)

    if events:
        # Convert the ISO 8601 date times to a datetime object
        # This allows the Django template to format the value nicely
        for event in events["value"]:
            event["start"]["dateTime"] = dateutil.parser.parse(
                event["start"]["dateTime"]
            )
            event["end"]["dateTime"] = dateutil.parser.parse(event["end"]["dateTime"])

        context["events"] = events["value"]

    return render(request, "tutorial/calendar.html", context)


def docs(request):
    graph_client = OAuth2Session(token=get_token(request))
    ret = graph_client.get(
        "https://graph.microsoft.com/v1.0/me/drive/root:/Gino:/children"
    )
    context = initialize_context(request)
    context["data"] = json.dumps(ret.json(), indent=2)
    return render(request, "tutorial/onedrive.html", context)


# https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_post_children?view=odsp-graph-online
def create_dir(request):
    graph_client = OAuth2Session(token=get_token(request))
    payload = {
        "name": "Gino",
        "folder": {},
        "@microsoft.graph.conflictBehavior": "rename",
    }
    # @microsoft.graph.conflictBehavior -->
    # https://docs.microsoft.com/en-us/onedrive/developer/rest-api/resources/driveitem?view=odsp-graph-online#instance-attributes
    ret = graph_client.post(
        "https://graph.microsoft.com/v1.0/me/drive/root/children", json=payload
    )
    context = initialize_context(request)
    context["data"] = json.dumps(ret.json(), indent=2)
    return render(request, "tutorial/onedrive.html", context)


# https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_put_content?view=odsp-graph-online
def upload_new_file(request):
    graph_client = OAuth2Session(token=get_token(request))
    file_name = "".join(random.sample("asdfghjklq", 4)) + ".docx"
    ret = graph_client.put(
        f"https://graph.microsoft.com/v1.0/me/drive/root:/Gino/{file_name}:/content",
        headers={
            "Content-Type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        },
        # warning: can not use lib requests kwarg `files`
        data=open("temp.docx", "rb").read(),
    )
    context = initialize_context(request)
    context["data"] = json.dumps(ret.json(), indent=2)
    return render(request, "tutorial/onedrive.html", context)
